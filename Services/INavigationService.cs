﻿namespace WpfApp2.Services;

public interface INavigationService
{
    void NavigateTo(object page);
}
