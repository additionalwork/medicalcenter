﻿using System;
using System.Windows.Input;

namespace WpfApp2.ViewModels;

internal class RelayCommand : ICommand
{
    private readonly Action _execute;
    
    public RelayCommand(Action execute)
    {
        _execute = execute;
    }

    public event EventHandler CanExecuteChanged;

    public bool CanExecute(object parameter)
    {
        return true;
    }

    public void Execute(object parameter)
    {
        _execute.Invoke();
    }
}
