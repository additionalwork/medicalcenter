﻿using WpfApp2.Repository.Models;

namespace WpfApp2.ViewModels;

public class NonAdminViewModel: ViewModelBase
{
    private string _role;
    private User _user;
    
    public NonAdminViewModel(User user)
    {
        _user = user;

        if (user.RoleId == 1) { _role = "Administrator"; }
        if (user.RoleId == 2) { _role = "Doctor"; }
        if (user.RoleId == 3) { _role = "Operator"; }
    }

    public string CurrentUsername
    {
        get => _user.Username;
    }

    public string Role
    {
        get => _role;
    }
}
