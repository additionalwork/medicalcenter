﻿using System.Windows.Controls;
using WpfApp2.Services;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for AddStudySeriesView.xaml
/// </summary>
public partial class AddStudySeriesView : UserControl
{
    public AddStudySeriesView(int id, INavigationService n)
    {
        InitializeComponent();
        DataContext = new AddStudySeriesViewModel(id, n);
    }
}
