﻿using System.Windows.Controls;
using WpfApp2.Services;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for SearchPatientView.xaml
/// </summary>
public partial class SearchPatientView : UserControl
{
    public SearchPatientView(INavigationService n)
    {
        InitializeComponent();
        DataContext = new SearchPatientViewModel(n);
    }
}
