﻿using System.Windows.Controls;
using WpfApp2.Repository;
using WpfApp2.Repository.Models;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for AdminView.xaml
/// </summary>
public partial class AdminView : UserControl
{
    MoDbContext _db;
    public AdminView(User user)
    {
        InitializeComponent();
        _db = new MoDbContext();
        DataContext = new AdminViewModel(_db, user);
    }
}
