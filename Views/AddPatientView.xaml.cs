﻿using System.Windows.Controls;
using WpfApp2.Services;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for AddPatientView.xaml
/// </summary>
public partial class AddPatientView : UserControl
{
    public AddPatientView(INavigationService s)
    {
        InitializeComponent();
        DataContext = new AddPatientViewModel(s);
    }
}
