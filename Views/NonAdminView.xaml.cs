﻿using System.Windows.Controls;
using WpfApp2.Repository.Models;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for NonAdminView.xaml
/// </summary>
public partial class NonAdminView : UserControl
{
    public NonAdminView(User user)
    {
        InitializeComponent();
        DataContext = new NonAdminViewModel(user);
    }
}
