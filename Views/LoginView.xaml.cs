﻿using System.Windows.Controls;
using WpfApp2.Repository;
using WpfApp2.Services;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for LoginView.xaml
/// </summary>
public partial class LoginView : Page
{
    MoDbContext context;

    public LoginView(MoDbContext _context, INavigationService n)
    {
        InitializeComponent();
        context = _context;               
        DataContext = new LoginViewModel(context, n);  
    }
}
