﻿using System.Windows.Controls;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for AddDataToPatientUserControl.xaml
/// </summary>
public partial class AddDataToPatientView : UserControl
{
    public AddDataToPatientView(int PatientID, int seriesID)
    {
        InitializeComponent();
        DataContext = new AddDataToPatientViewModel(PatientID, seriesID);
    }
}
