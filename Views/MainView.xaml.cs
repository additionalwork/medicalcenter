﻿using System.Windows.Controls;
using WpfApp2.Repository;
using WpfApp2.Repository.Models;
using WpfApp2.Services;
using WpfApp2.ViewModels;

namespace WpfApp2.Views;

/// <summary>
/// Interaction logic for MainView.xaml
/// </summary>
public partial class MainView : Page, INavigationService
{
    User _user;
    
    public MainView(MoDbContext context, User user, INavigationService mainWindowNavigation)
    {
        InitializeComponent();
        _user = user;
        DataContext = new MainViewModel(context, this, _user, mainWindowNavigation);
    }

    public void NavigateTo(object page)
    {
        ContentArea.Content = page;
    }
}
