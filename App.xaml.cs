﻿using log4net;
using System.Windows;
using Unity;
using WpfApp2.Repository;

namespace WpfApp2;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
    IUnityContainer container;
    private static readonly ILog log = LogManager.GetLogger(typeof(App));

    public App()
    {
        container = new UnityContainer();
        container.RegisterType<MoDbContext>();
    }
    
    protected override void OnStartup(StartupEventArgs e)
    {
        log4net.Config.XmlConfigurator.Configure();
        log.Info("        =============  Started Application  =============        ");
        base.OnStartup(e);
        Startup += App_Startup;
    }

    private void App_Startup(object sender, StartupEventArgs e)
    {
        var mainW = new MainWindow(container);
        mainW.Show();
    }
}
